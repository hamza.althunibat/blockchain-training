#!/bin/bash

function createOrderer() {
    echo "Enroll the CA Admin"
    sleep 2
    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz
    export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/ordererOrganizations/dm.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://admin:adminpw@localhost:10054 --caname ca-orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/localhost-10054-ca-orderer.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/localhost-10054-ca-orderer.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/localhost-10054-ca-orderer.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/localhost-10054-ca-orderer.pem
        OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml

    echo "Register orderer"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name orderer --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo "Register orderer2"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name orderer2 --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo "Register orderer3"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name orderer3 --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo "Register orderer4"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name orderer4 --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo "Register orderer5"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name orderer5 --id.secret ordererpw --id.type orderer --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    echo "Register the orderer admin"
    ./bin/fabric-ca-client register --caname ca-orderer --id.name ordererAdmin --id.secret ordererAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts
    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/users
    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz

    echo "Generate the admin msp"
    ./bin/fabric-ca-client enroll -u https://ordererAdmin:ordererAdminpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/ordererOrg/tls-cert.pem
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz/msp/config.yaml

    # -----------------------------------------------------------------------
    #  Orderer 1

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz

    echo "Generate the orderer msp"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/msp --csr.hosts orderer.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/msp/config.yaml

    echo "Generate the orderer-tls certificates"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts orderer.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    # -----------------------------------------------------------------------
    #  Orderer 2

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz

    echo "Generate the orderer msp"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/msp --csr.hosts orderer2.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/msp/config.yaml

    echo "Generate the orderer-tls certificates"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts orderer2.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer2.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    # -----------------------------------------------------------------------
    #  Orderer 3

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz

    echo "Generate the orderer msp"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/msp --csr.hosts orderer3.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/msp/config.yaml

    echo "Generate the orderer-tls certificates"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts orderer3.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer3.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    # -----------------------------------------------------------------------
    #  Orderer 4

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz

    echo "Generate the orderer msp"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/msp --csr.hosts orderer4.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/msp/config.yaml

    echo "Generate the orderer-tls certificates"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts orderer4.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer4.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    # -----------------------------------------------------------------------
    #  Orderer 5

    mkdir -p organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz

    echo "Generate the orderer msp"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/msp --csr.hosts orderer5.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/msp/config.yaml

    echo "Generate the orderer-tls certificates"
    ./bin/fabric-ca-client enroll -u https://orderer:ordererpw@localhost:10054 --caname ca-orderer -M ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts orderer5.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DMOrderer/tls-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

    cp ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/orderers/orderer5.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/ordererOrganizations/dm.godwit.xyz/msp/tlscacerts/tlsca.dm.godwit.xyz-cert.pem

}

function createDmOrg() {
    echo "Enroll the CA admin"
    sleep 2
    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/

    export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/dm.godwit.xyz/

    ./bin/fabric-ca-client enroll -u https://admin:adminpw@localhost:7054 --caname ca-dm --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/localhost-7054-ca-dm.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/localhost-7054-ca-dm.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/localhost-7054-ca-dm.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/localhost-7054-ca-dm.pem
        OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/config.yaml

    echo "Register peer0"
    ./bin/fabric-ca-client register --caname ca-dm --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    echo "Register user"
    ./bin/fabric-ca-client register --caname ca-dm --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    echo "Register the org admin"
    ./bin/fabric-ca-client register --caname ca-dm --id.name dmAdmin --id.secret dmAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/peers

    echo "Generate the peer0 msp"
    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-dm -M ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/msp --csr.hosts peer0.dm.godwit.xyz --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/msp/config.yaml

    echo "Generate the peer0-tls certificates"
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:7054 --caname ca-dm -M ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls --enrollment.profile tls --csr.hosts peer0.dm.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/signcerts/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/keystore/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/tlscacerts/ca.crt

    mkdir -p ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/tlsca
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/tlsca/tlsca.dm.godwit.xyz-cert.pem

    mkdir -p ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/ca
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/peers/peer0.dm.godwit.xyz/msp/cacerts/* ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/ca/ca.dm.godwit.xyz-cert.pem

    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/users

    echo "Generate the user msp"
    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/users/User1@dm.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://user1:user1pw@localhost:7054 --caname ca-dm -M ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/users/User1@dm.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/users/User1@dm.godwit.xyz/msp/config.yaml

    echo "Generate the org admin msp"
    mkdir -p organizations/peerOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://dmAdmin:dmAdminpw@localhost:7054 --caname ca-dm -M ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DM/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dm.godwit.xyz/users/Admin@dm.godwit.xyz/msp/config.yaml

}

function createDcOrg() {
    echo "Enroll the CA admin"
    sleep 2
    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/

    export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/dc.godwit.xyz/

    ./bin/fabric-ca-client enroll -u https://admin:adminpw@localhost:8054 --caname ca-dc --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/localhost-8054-ca-dc.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/localhost-8054-ca-dc.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/localhost-8054-ca-dc.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/localhost-8054-ca-dc.pem
        OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/config.yaml

    echo "Register peer0"
    ./bin/fabric-ca-client register --caname ca-dc --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    echo "Register user"
    ./bin/fabric-ca-client register --caname ca-dc --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    echo "Register the org admin"
    ./bin/fabric-ca-client register --caname ca-dc --id.name dcAdmin --id.secret dcAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/peers

    echo "Generate the peer0 msp"
    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-dc -M ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/msp --csr.hosts peer0.dc.godwit.xyz --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/msp/config.yaml

    echo "Generate the peer0-tls certificates"
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:8054 --caname ca-dc -M ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls --enrollment.profile tls --csr.hosts peer0.dc.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/signcerts/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/keystore/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/tlscacerts/ca.crt

    mkdir -p ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/tlsca
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/tlsca/tlsca.dc.godwit.xyz-cert.pem

    mkdir -p ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/ca
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/peers/peer0.dc.godwit.xyz/msp/cacerts/* ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/ca/ca.dc.godwit.xyz-cert.pem

    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/users

    echo "Generate the user msp"
    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/users/User1@dc.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://user1:user1pw@localhost:8054 --caname ca-dc -M ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/users/User1@dc.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/users/User1@dc.godwit.xyz/msp/config.yaml

    echo "Generate the org admin msp"
    mkdir -p organizations/peerOrganizations/dc.godwit.xyz/users/Admin@dc.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://dcAdmin:dcAdminpw@localhost:8054 --caname ca-dc -M ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/users/Admin@dc.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DC/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dc.godwit.xyz/users/Admin@dc.godwit.xyz/msp/config.yaml

}

function createDpwOrg() {
    echo "Enroll the CA admin"
    sleep 2
    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/

    export FABRIC_CA_CLIENT_HOME=${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/

    ./bin/fabric-ca-client enroll -u https://admin:adminpw@localhost:9054 --caname ca-dpw --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    echo 'NodeOUs:
    Enable: true
    ClientOUIdentifier:
        Certificate: cacerts/localhost-9054-ca-dpw.pem
        OrganizationalUnitIdentifier: client
    PeerOUIdentifier:
        Certificate: cacerts/localhost-9054-ca-dpw.pem
        OrganizationalUnitIdentifier: peer
    AdminOUIdentifier:
        Certificate: cacerts/localhost-9054-ca-dpw.pem
        OrganizationalUnitIdentifier: admin
    OrdererOUIdentifier:
        Certificate: cacerts/localhost-9054-ca-dpw.pem
        OrganizationalUnitIdentifier: orderer' >${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/config.yaml

    echo "Register peer0"
    ./bin/fabric-ca-client register --caname ca-dpw --id.name peer0 --id.secret peer0pw --id.type peer --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    echo "Register user"
    ./bin/fabric-ca-client register --caname ca-dpw --id.name user1 --id.secret user1pw --id.type client --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    echo "Register the org admin"
    ./bin/fabric-ca-client register --caname ca-dpw --id.name dpwAdmin --id.secret dpwAdminpw --id.type admin --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/peers

    echo "Generate the peer0 msp"
    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:9054 --caname ca-dpw -M ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/msp --csr.hosts peer0.dpw.godwit.xyz --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/msp/config.yaml

    echo "Generate the peer0-tls certificates"
    ./bin/fabric-ca-client enroll -u https://peer0:peer0pw@localhost:9054 --caname ca-dpw -M ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls --enrollment.profile tls --csr.hosts peer0.dpw.godwit.xyz --csr.hosts localhost --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem

    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/ca.crt
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/signcerts/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/server.crt
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/keystore/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/server.key

    mkdir -p ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/tlscacerts
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/tlscacerts/ca.crt

    mkdir -p ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/tlsca
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/tls/tlscacerts/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/tlsca/tlsca.dpw.godwit.xyz-cert.pem

    mkdir -p ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/ca
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/peers/peer0.dpw.godwit.xyz/msp/cacerts/* ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/ca/ca.dpw.godwit.xyz-cert.pem

    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/users

    echo "Generate the user msp"
    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/users/User1@dpw.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://user1:user1pw@localhost:9054 --caname ca-dpw -M ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/users/User1@dpw.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/users/User1@dpw.godwit.xyz/msp/config.yaml

    echo "Generate the org admin msp"
    mkdir -p organizations/peerOrganizations/dpw.godwit.xyz/users/Admin@dpw.godwit.xyz
    ./bin/fabric-ca-client enroll -u https://dpwAdmin:dpwAdminpw@localhost:9054 --caname ca-dpw -M ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/users/Admin@dpw.godwit.xyz/msp --tls.certfiles ${PWD}/organizations/fabric-ca/DPW/tls-cert.pem
    cp ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/msp/config.yaml ${PWD}/organizations/peerOrganizations/dpw.godwit.xyz/users/Admin@dpw.godwit.xyz/msp/config.yaml

}
