#!/bin/bash

CORE_PEER_LOCALMSPID=$1

peer channel update -o orderer.dm.godwit.xyz:7050 -c irschannel -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls --cafile $ORDERER_CA
