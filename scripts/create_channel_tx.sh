#!/bin/bash
export FABRIC_CFG_PATH=${PWD}/configtx

./bin/configtxgen -profile IrsChannel -outputCreateChannelTx ./channel-artifacts/irschannel.tx -channelID irschannel

for orgmsp in DM DC DPW; do

	echo "Generating anchor peer update transaction for ${orgmsp}"
	./bin/configtxgen -profile IrsChannel -outputAnchorPeersUpdate ./channel-artifacts/${orgmsp}anchors.tx -channelID irschannel -asOrg ${orgmsp}

	done